import React, {
	DetailedHTMLProps,
	FunctionComponent,
	HTMLAttributes,
	ReactNode,
} from 'react';
import styles from './withSectionLayout.module.scss';

interface SectionLayoutProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement> {
	children: ReactNode;
}

const SectionLayout = ({
	children,
	...props
}: SectionLayoutProps): JSX.Element => {
	return (
		<section {...props} className={styles.sectionLayout}>
			{children}
		</section>
	);
};

export const withSectionLayout = <T extends Record<string, unknown>>(
	Component: FunctionComponent<T>
) => {
	return function withSectionLayoutComponent(props: T): JSX.Element {
		return (
			<SectionLayout>
				<Component {...props} />
			</SectionLayout>
		);
	};
};

export default withSectionLayout;
