import React, {
	DetailedHTMLProps,
	FunctionComponent,
	HTMLAttributes,
	ReactNode,
} from 'react';
import styles from './withPageLayout.module.scss';

interface PageLayoutProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	children: ReactNode;
}

const PageLayout = ({ children, ...props }: PageLayoutProps): JSX.Element => {
	return (
		<div {...props} className={styles.pageLayout}>
			{children}
		</div>
	);
};

export const withPageLayout = <T extends Record<string, unknown>>(
	Component: FunctionComponent<T>
) => {
	return function withPageLayoutComponent(props: T): JSX.Element {
		return (
			<PageLayout>
				<Component {...props} />
			</PageLayout>
		);
	};
};

export default withPageLayout;
