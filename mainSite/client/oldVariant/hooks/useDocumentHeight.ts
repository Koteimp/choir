import React, { useEffect, useState } from 'react';

const useDocumentHeight = () => {
	const [height, setHeight] = useState(0);
	const [windowHeight, setWindowHeight] = useState(0);
	useEffect(() => {
		function updateHeight() {
			setHeight(document.documentElement.scrollHeight);
		}
		function updateWindowHeight() {
			setWindowHeight(window.innerHeight);
		}
		window.addEventListener('resize', () => {
			updateWindowHeight();
			updateHeight();
		});
		updateHeight();
		updateWindowHeight();
		return () =>
			window.removeEventListener('resize', () => {
				updateWindowHeight();
				updateHeight();
			});
	}, []);
	return { height, windowHeight };
};

export default useDocumentHeight;
