import React, {
	DetailedHTMLProps,
	Dispatch,
	SetStateAction,
	HTMLAttributes,
	useEffect,
	useRef,
	useState,
} from 'react';
import cn from 'classnames';
import {
	IoPauseOutline,
	IoPlayOutline,
	IoVolumeLowOutline,
} from 'react-icons/io5';
import styles from './AudioPlayer.module.scss';
import { motion, Variants } from 'framer-motion';

interface AudioPlayerProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	activeTrack?: string | null;
	setActiveTrack: Dispatch<SetStateAction<string | null>>;
}
const hoverVariants: Variants = {
	rest: {
		backgroundColor: 'var(--colorC)',
		transition: {},
	},
	hover: {
		backgroundColor: 'var(--colorMD)',
		transition: {},
	},
};
let audio: HTMLAudioElement;

const AudioPlayer = ({
	activeTrack,
	setActiveTrack,
	className,
	...props
}: AudioPlayerProps): JSX.Element => {
	const [isPlaying, setIsPlaying] = useState<boolean>(false);
	const [currentTime, setCurrentTime] = useState<number>(0);
	const [duration, setDuration] = useState<number>(0);
	const [volume, setVolume] = useState<number>(50);

	useEffect(() => {
		if (!audio) {
			audio = new Audio();
			setAudio();
		} else setAudio();
	}, [activeTrack]);

	useEffect(() => {
		return () => {
			setIsPlaying(false);
			audio.pause();
			setActiveTrack(null);
			setCurrentTime(0);
			setDuration(0);
		};
	}, []);

	useEffect(() => {
		if (audio.currentTime == audio.duration) {
			setCurrentTime(0);
			setIsPlaying(false);
			setAudioBg(0);
		}
	}, [currentTime]);

	const volumeRef = useRef<HTMLInputElement | any>(null);
	const audioRef = useRef<HTMLInputElement | any>(null);

	const setAudio = () => {
		if (activeTrack) {
			audio.src = activeTrack;
			audio.volume = volume / 100;
			setVolumeBg(volume);
			audio.onloadedmetadata = () => {
				setDuration(Math.floor(audio.duration));
			};
			audio.ontimeupdate = () => {
				setCurrentTime(audio.currentTime);
				setAudioBg(audio.currentTime);
			};
		}
	};

	const playAudio = () => {
		setIsPlaying(!isPlaying);
		if (!isPlaying) {
			audio.play();
		} else {
			audio.pause();
		}
	};

	const setTrack = (str: number) => {
		if (activeTrack !== `/audioFiles/DC${str}.mp3`) {
			setIsPlaying(false);
			if (str === 1) {
				setActiveTrack('/audioFiles/DC1.mp3');
			} else if (str === 2) {
				setActiveTrack('/audioFiles/DC2.mp3');
			} else if (str === 3) {
				setActiveTrack('/audioFiles/DC3.mp3');
			}
		}
	};

	const formatTime = (seconds: number): string => {
		const mins = Math.floor(seconds / 60);
		const ftMin = mins < 10 ? `0${mins}` : `${mins}`;
		const secs = Math.floor(seconds % 60);
		const ftSec = secs < 10 ? `0${secs}` : `${secs}`;
		return `${ftMin}:${ftSec}`;
	};

	const changeVolume = (e: React.ChangeEvent<HTMLInputElement>) => {
		audio.volume = Number(e.target.value) / 100;
		setVolume(Number(e.target.value));
		setVolumeBg(audio.volume * 100);
	};

	const changeCurrentTime = (e: React.ChangeEvent<HTMLInputElement>) => {
		audio.currentTime = Number(e.target.value);
		setCurrentTime(Number(e.target.value));
	};

	const setVolumeBg = (num: number) => {
		if (volumeRef) {
			volumeRef.current?.style.setProperty(
				'--volPercent',
				`${
					((num - volumeRef.current?.min) /
						(volumeRef.current?.max - volumeRef.current?.min)) *
					100
				}%`
			);
		}
	};
	const setAudioBg = (time: number) => {
		if (audioRef) {
			const oneStep =
				audioRef.current?.clientWidth / Math.floor(audio.duration); //
			audioRef.current?.style.setProperty(
				'--audioPercent',
				`${Math.floor(oneStep * time) || 0}px`
			);
		}
	};

	return (
		<div className={cn(styles.audioPlayer, className)} {...props}>
			<div className={styles.player}>
				<div className={styles.playerContainer}>
					<button className={styles.playButton} onClick={playAudio}>
						{!isPlaying ? <IoPlayOutline /> : <IoPauseOutline />}
					</button>
					<div className={styles.currentTime}>
						{formatTime(currentTime)}
					</div>
					{'/'}
					<div className={styles.durationTime}>
						{formatTime(duration)}
					</div>
					<div className={styles.progressContainer}>
						<input
							type='range'
							className={styles.progressBar}
							onChange={changeCurrentTime}
							min={0}
							max={duration}
							value={currentTime}
							ref={audioRef}
							step={1}
						/>
					</div>

					{/* <div>
						<div className={styles.volumeContainer}>
							<div className={styles.volumeIcon}>
								<IoVolumeLowOutline />
							</div>
							<input
								type='range'
								className={styles.volumeBar}
								onChange={changeVolume}
								min={0}
								max={100}
								value={volume}
								ref={volumeRef}
								step={1}
							/>
						</div>
					</div> */}
				</div>
				<div className={styles.tracksContainer}>
					{[
						{
							id: 1,
							title: 'Д. Яичков. Пасхальные стихиры знаменного распева',
							alias: '/audioFiles/DC1.mp3',
						},
						{
							id: 2,
							title: 'Полихронион. Многая лета на греческом',
							alias: '/audioFiles/DC2.mp3',
						},
						{
							id: 3,
							title: 'Д. Яичков. Взбранной воеводе',
							alias: '/audioFiles/DC3.mp3',
						},
					].map((track) => {
						return (
							<motion.div
								key={track.id}
								className={styles.trackItem}
								onClick={() => {
									setTrack(track.id);
								}}
								whileHover={
									activeTrack !== track.alias
										? { background: 'var(--colorMD)' }
										: {}
								}
								animate={
									activeTrack === track.alias
										? { background: 'var(--colorM)' }
										: { background: 'var(--colorC)' }
								}
							>
								<span
									className={styles.trackIcon}
									onClick={() => playAudio()}
								>
									{activeTrack === track.alias &&
									isPlaying ? (
										<IoPauseOutline />
									) : (
										<IoPlayOutline />
									)}
								</span>
								<button
									onClick={() => setTrack(track.id)}
									className={styles.trackSelectButton}
								>
									{track.title}
								</button>
							</motion.div>
						);
					})}
				</div>
			</div>
		</div>
	);
};

export default AudioPlayer;
