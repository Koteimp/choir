import {
	AnimatePresence,
	AnimateSharedLayout,
	HTMLMotionProps,
	motion,
	Variants,
} from 'framer-motion';
import React, { useState } from 'react';
import AudioPlayer from '../AudioPlayer/AudioPlayer';
import styles from './Modal.module.scss';

interface ModalProps extends HTMLMotionProps<'div'> {}

const modalVariants: Variants = {
	visible: {
		opacity: 1,
	},
	hidden: {
		opacity: 0,
	},
};

const Modal = ({}: ModalProps): JSX.Element => {
	const [isOpen, setIsOpen] = useState<boolean | null>(null);
	const [activeTrack, setActiveTrack] = useState<string | null>(
		'/audioFiles/DC1.mp3'
	);

	const handleClose = () => {
		setActiveTrack(null);
		setIsOpen(false);
	};
	const handleOpen = () => {
		setActiveTrack('/audioFiles/DC1.mp3');
		setIsOpen(true);
	};

	return (
		<AnimateSharedLayout >
			<motion.div className={styles.wrapper}>
				<motion.div className={styles.buttonContainer} layout>
					<motion.div
						className={styles.playerContainer}
						layoutId={'container'}
					>
						<motion.button
							className={styles.containerButton}
							onClick={handleOpen}
							layoutId={'button'}
							aria-label='открыть плеер'
						>
							<svg
								stroke='currentColor'
								fill='currentColor'
								strokeWidth='0'
								viewBox='0 0 512 512'
								height='1em'
								width='1em'
							>
								<path
									strokeMiterlimit='10'
									strokeWidth='32'
									d='M112 111v290c0 17.44 17 28.52 31 20.16l247.9-148.37c12.12-7.25 12.12-26.33 0-33.58L143 90.84c-14-8.36-31 2.72-31 20.16z'
								></path>
							</svg>
						</motion.button>
					</motion.div>
				</motion.div>
				<AnimatePresence exitBeforeEnter>
					{isOpen && (
						<motion.div
							className={styles.modalContainer}
							layout
							initial='hidden'
							animate='visible'
							exit='hidden'
							variants={modalVariants}
						>
							<motion.div
								className={styles.modal}
								layoutId={'container'}
							>
								<motion.button
									className={styles.modalButton}
									onClick={handleClose}
									layoutId={'button'}
								>
									<svg
										stroke='currentColor'
										fill='currentColor'
										strokeWidth='0'
										viewBox='0 0 512 512'
										height='1em'
										width='1em'
									>
										<path
											fill='none'
											strokeMiterlimit='10'
											strokeWidth='32'
											d='M448 256c0-106-86-192-192-192S64 150 64 256s86 192 192 192 192-86 192-192z'
										></path>
										<path
											fill='none'
											strokeLinecap='round'
											strokeLinejoin='round'
											strokeWidth='32'
											d='M320 320L192 192m0 128l128-128'
										></path>
									</svg>
								</motion.button>
								<AudioPlayer
									activeTrack={activeTrack}
									setActiveTrack={setActiveTrack}
									className={styles.player}
								/>
							</motion.div>
						</motion.div>
					)}
				</AnimatePresence>
			</motion.div>
		</AnimateSharedLayout>
	);
};

export default Modal;
