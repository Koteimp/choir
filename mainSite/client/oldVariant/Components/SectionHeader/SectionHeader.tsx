import React, { ForwardedRef, forwardRef } from 'react';
// import styles from './SectionHeader.module.scss';
import * as dataText from '../../../Components/data';

import cn from 'classnames';
import { motion, HTMLMotionProps, Variants } from 'framer-motion';
// import { useInView } from 'react-intersection-observer';

interface SectionHeaderProps extends HTMLMotionProps<'div'> {
	text?: string;
	header: string;
	section: 'main' | 'secondary';
	shouldAnimate?: boolean;
}

const containerH2Variants: Variants = {
	shown: {
		transition: {
			staggerChildren: 0.5,
			delayChildren: 0.35,
		},
	},
	hidden: {
		transition: {},
	},
};
const wordH2Animation: Variants = {
	hidden: {
		opacity: 0,
		y: 50,
	},
	shown: {
		opacity: 1,
		y: 0,
		transition: {
			ease: [0.26, 0.67, 0.48, 0.91],
			duration: 0.6,
		},
	},
};

const containerH1Variants: Variants = {
	shown: {
		y: [-70, -70, 0],
		transition: {
			times: [0, 0.8, 1],
			staggerChildren: 0.4,
			duration: 2.5,
		},
	},
	hidden: { y: -70, transition: {} },
};
const wordH1Animation: Variants = {
	hidden: {},
	shown: {
		transition: {
			ease: [0.6, 0.01, -0.05, 0.95],
			staggerChildren: 0.05,
		},
	},
};
const letterH1Animation: Variants = {
	hidden: {
		y: 400,
	},
	shown: {
		y: 0,
		transition: {
			ease: [0.6, 0.01, -0.05, 0.95],
			duration: 0.5,
		},
	},
};

const subHeaderAnimation: Variants = {
	hidden: {
		opacity: 0,
	},
	shown: {
		opacity: 1,
		transition: {
			delay: 2.95,
			duration: 0.5,
		},
	},
};

const SectionHeader = (
	{
		text,
		header,
		section,
		shouldAnimate = true,
		className,
		...props
	}: SectionHeaderProps,
	ref: ForwardedRef<HTMLDivElement>
) => {
	const allText = dataText.allText;

	// const words = allText.sectionHeaders[0].header
	// 	.split(' ')
	// 	.map((word, index) => {
	// 		return (
	// 			<motion.span
	// 				key={index}
	// 				className={styles.word}
	// 				variants={wordH1Animation}
	// 			>
	// 				{[...word].map((letter, indexLt) => {
	// 					return (
	// 						<motion.span
	// 							key={indexLt}
	// 							variants={letterH1Animation}
	// 						>
	// 							{letter}
	// 						</motion.span>
	// 					);
	// 				})}
	// 			</motion.span>
	// 		);
	// 	});

	// const [ref1, inView1] = useInView();
	return (
		<motion.div
			// className={cn(styles.sectionHeader, className)}
			{...props}
			// ref={ref}
		>
			{/* {section === 'main' ? (
				<div className={styles.headerContainerH1}>
					<motion.h6
						className={styles.textWithH1}
						variants={subHeaderAnimation}
					>
						{text}
					</motion.h6>
					<div className={styles.headerH1Wrapper}>
						<motion.h1
							className={styles.headerH1}
							variants={containerH1Variants}
						>
							{words}
						</motion.h1>
					</div>
				</div>
			) : (
				<motion.div
					className={styles.headerContainerH2}
					variants={containerH2Variants}
					// ref={ref1}
					// animate={
					// 	inView1 ? 'shown' : shouldAnimate ? 'hidden' : 'shown'
					// }
					initial={shouldAnimate ? 'hidden' : 'shown'}
				>
					<div className={styles.headerH2Wrapper}>
						<motion.h2
							className={styles.headerH2}
							variants={wordH2Animation}
						>
							{header}
						</motion.h2>
					</div>
					{text && (
						<motion.p
							className={styles.textWithH2}
							variants={wordH2Animation}
						>
							{text}
						</motion.p>
					)}
				</motion.div>
			)} */}
		</motion.div>
	);
};

export default motion(forwardRef(SectionHeader));
