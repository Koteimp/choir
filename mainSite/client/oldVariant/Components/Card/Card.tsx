import React, { ForwardedRef, forwardRef, useState } from 'react';
import styles from './Card.module.scss';
import cn from 'classnames';
import {
	AnimatePresence,
	HTMLMotionProps,
	motion,
	Variants,
} from 'framer-motion';

interface CardProps extends HTMLMotionProps<'div'> {
	text: string;
	header: string;
	darkText: string;
}

const cardContentVariants: Variants = {
	hidden: { opacity: 0, transition: { duration: 0.3 } },
	visible: { opacity: 1, transition: { delay: 0.3 } },
};

const Card = (
	{ text, header, darkText, className, ...props }: CardProps,
	ref: ForwardedRef<HTMLDivElement>
): JSX.Element => {
	return (
		<motion.div
			className={cn(styles.cardBlock, className)}
			{...props}
			ref={ref}
			initial={{ borderRadius: 20 }}
		>
			<motion.div className={styles.cardContainer}>
				<motion.p className={styles.cardHeader}>{header}</motion.p>
				<motion.div className={styles.cardContent}>
					<p className={styles.cardText}>{text}</p>
					<p className={styles.cardDarkText}>{darkText}</p>
				</motion.div>
			</motion.div>
		</motion.div>
	);
};

export default motion(forwardRef(Card));
