import React, { ForwardedRef, forwardRef } from 'react';
import styles from './SkillsSteps.module.scss';
import cn from 'classnames';
import { HTMLMotionProps, motion, Variants } from 'framer-motion';

interface SkillsStepsProps extends HTMLMotionProps<'div'> {
	// extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	text: string;
	header: string;
	skill: boolean;
}

const cardVariants: Variants = {
	shown: {
		opacity: 1,
		y: '0px',
		transition: {
			ease: 'easeOut',
		},
	},
	hidden: { opacity: 0, y: '50px', transition: {} },
};

const SkillsSteps = (
	{ text, header, skill, className, ...props }: SkillsStepsProps,
	ref: ForwardedRef<HTMLDivElement>
) => {
	return (
		<motion.div
			className={cn(styles.skillsBlock, className)}
			{...props}
			ref={ref}
		>
			<motion.div
				className={styles.skillsContainer}
				variants={cardVariants}
			>
				{skill ? (
					<p className={styles.skillsHeader}>{header}</p>
				) : (
					<p className={styles.stepsHeader}>{header}</p>
				)}
				<p className={styles.skillsText}>{text}</p>
			</motion.div>
		</motion.div>
	);
};

export default motion(forwardRef(SkillsSteps));
