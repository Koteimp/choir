import React, {
	DetailedHTMLProps,
	ForwardedRef,
	forwardRef,
	HTMLAttributes,
} from 'react';
import styles from './PhaseCard.module.scss';
import cn from 'classnames';
import {
	AnimatePresence,
	HTMLMotionProps,
	motion,
	Variants,
} from 'framer-motion';

interface PhaseCardProps extends HTMLMotionProps<'div'> {
	note?: string;
	header: string;
	list: string[];
	noteExists: boolean;
}

const containerVariants: Variants = {
	shown: {
		transition: {
			staggerChildren: 0.5,
		},
	},
	hidden: { transition: {} },
};
const containerVariants2: Variants = {
	shown: {
		transition: {
			staggerChildren: 0.3,
		},
	},
	hidden: { transition: {} },
};
const contentVariants: Variants = {
	shown: {
		opacity: 1,
		y: 0,
		transition: {
			ease: 'easeOut',
		},
	},
	hidden: { opacity: 0, y: 50, transition: {} },
};

const PhaseCard = (
	{ note, header, list, noteExists, className, ...props }: PhaseCardProps,
	ref: ForwardedRef<HTMLDivElement>
) => {
	return (
		<motion.div className={cn(styles.phaseCardBlock, className)} {...props}>
			<motion.div
				className={styles.phaseCardContainer}
				variants={containerVariants}
			>
				<motion.p
					className={styles.phaseCardHeader}
					variants={contentVariants}
				>
					{header}
				</motion.p>
				<motion.ul
					className={styles.listContainer}
					variants={containerVariants2}
				>
					<AnimatePresence>
						{list.map((item, index) => (
							<motion.li
								className={styles.listItem}
								key={index}
								variants={contentVariants}
							>
								{item}
							</motion.li>
						))}
					</AnimatePresence>
				</motion.ul>
				{noteExists && note && (
					<motion.p
						className={styles.note}
						variants={contentVariants}
					>
						{note}
					</motion.p>
				)}
			</motion.div>
		</motion.div>
	);
};

export default motion(forwardRef(PhaseCard));
