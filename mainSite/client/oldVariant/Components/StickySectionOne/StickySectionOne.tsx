import React, { useContext, useEffect, useRef } from 'react';
import styles from './StickySectionOne.module.scss';
import * as dataText from '../../../Components/data';
import { motion, useAnimation, Variants } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
import { useTransform, useViewportScroll } from 'framer-motion';
import SectionHeader from '../SectionHeader/SectionHeader';
import Modal from '../Modal/Modal';

interface StickySectionProps {}

const containerVariants: Variants = {
	initial: {
		transition: { duration: 0.2 },
	},
	shown: {
		transition: {
			staggerChildren: 0.5,
		},
	},
	hidden: { transition: { duration: 0.2 } },
};

const contentVariants: Variants = {
	shown: {
		y: '0px',
		opacity: 1,
		transition: { ease: [0.26, 0.67, 0.48, 0.91] },
	},
	hidden: { y: '100px', opacity: 0, transition: {} },
};

const StickySection1 = ({ ...props }: StickySectionProps): JSX.Element => {
	const allText = dataText.allText;
	//высота секции
	const heightMax = 3000;
	const wrapperRef = useRef<HTMLDivElement | any>(null);
	const contentRef = useRef<HTMLDivElement | any>(null);
	useEffect(() => {
		if (wrapperRef && contentRef) {
			const height = contentRef.current.offsetHeight + heightMax;
			wrapperRef.current.style.height = `${height}px`;
		}
	}, [wrapperRef, contentRef, heightMax]);
	let percent = 0;
	//анимация при скролле контейнер идет снизу вверх
	const { scrollYProgress } = useViewportScroll();
	const yScroll = useTransform(
		scrollYProgress,
		[0, percent],
		['0%', '-100%']
	);
	//анимация фреймов при попадании в поле видимости
	const [ref1, inView1] = useInView({
		initialInView: true,
	});

	const frame1Controls = useAnimation();

	const sque = async () => {
		if (inView1) {
			frame1Controls.start('shown');
		} else {
			frame1Controls.start('hidden');
		}
	};

	useEffect(() => {
		sque();
	}, [inView1]);

	return (
		<div
			className={styles.stickySectionWrapper}
			ref={wrapperRef}
			{...props}
		>
			<div className={styles.stickySectionlContent} ref={contentRef}>
				<div className={styles.bgWrap}>
					{/* <img alt='forest' src='/mountains.jpg' /> */}
				</div>
				<motion.div className={styles.content} style={{ y: yScroll }}>
					<div className={styles.frame1}>
						<motion.div
							className={styles.frameContent}
							variants={containerVariants}
							animate={frame1Controls}
							ref={ref1}
							initial={'hidden'}
						>
							<SectionHeader
								text={allText.sectionHeaders[0].text}
								section={'main'}
								header={allText.sectionHeaders[0].header}
								variants={containerVariants}
							/>
							<div>
								<Modal />
							</div>
						</motion.div>
					</div>
					<div className={styles.frame2}>
						<motion.div
							className={styles.frameContent}
						>
							<SectionHeader
								text={allText.sectionHeaders[1].text}
								section={'secondary'}
								header={allText.sectionHeaders[1].header}
							/>
						</motion.div>
					</div>
					<div className={styles.frame3}>
						<motion.div
							className={styles.frameContent}
						>
							<SectionHeader
								text={allText.sectionHeaders[2].text}
								section={'secondary'}
								header={allText.sectionHeaders[2].header}
							/>
						</motion.div>
					</div>
				</motion.div>
			</div>
		</div>
	);
};

export default StickySection1;
