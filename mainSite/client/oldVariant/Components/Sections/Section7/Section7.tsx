import React, { DetailedHTMLProps, HTMLAttributes } from 'react';
import styles from './Section7.module.scss';
import cn from 'classnames';
import withSectionLayout from '../../../HOC/withSectionLayout';
import SectionHeader from '../../SectionHeader/SectionHeader';
import * as dataText from '../../../../Components/data';
import SkillsSteps from '../../SkillsSteps/SkillsSteps';
import Button from '../../Button/Button';
import { AnimatePresence, motion, Variants } from 'framer-motion';
import { useInView } from 'react-intersection-observer';

interface Section7Props
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}

const cardContainerVariants: Variants = {
	shown: {
		transition: {
			staggerChildren: 0.3,
			delayChildren: 0.5,
			ease: 'easeOut',
		},
	},
	hidden: { transition: {} },
};
const containerVariants: Variants = {
	shown: {
		transition: {
			staggerChildren: 0.5,
			delayChildren: 0.5,
			ease: 'easeOut',
		},
	},
	hidden: { transition: {} },
};
const contentVariants: Variants = {
	shown: {
		y: '0px',
		opacity: 1,
		transition: { ease: 'easeOut' },
	},
	hidden: { y: '100px', opacity: 0, transition: {} },
};

const Section7 = ({ className, ...props }: Section7Props): JSX.Element => {
	const allText = dataText.allText;
	const [ref2, inView2] = useInView();
	const [ref3, inView3] = useInView();

	return (
		<div className={cn(styles.section7, className)} {...props}>
			<SectionHeader
				header={allText.sectionHeaders[6].header}
				section='secondary'
				className={styles.sectionHeader}
			/>
			<AnimatePresence>
				<motion.div
					className={styles.cardContainer}
					variants={cardContainerVariants}
					ref={ref2}
					animate={inView2 ? 'shown' : 'hidden'}
					initial={'hidden'}
				>
					{allText.stepsText.map((item, index) => (
						<SkillsSteps
							key={index}
							text={item.text}
							header={item.header}
							skill={false}
						/>
					))}
				</motion.div>
			</AnimatePresence>
			<motion.div
				className={styles.textButtonContainer}
				variants={containerVariants}
				ref={ref3}
				animate={inView3 ? 'shown' : 'hidden'}
				initial={'hidden'}
			>
				<div className={styles.textContainer}>
					<motion.p
						className={styles.groupText}
						variants={contentVariants}
					>
						Берем не более 20 человек. Успей записаться
					</motion.p>
				</div>
				<div className={styles.buttonContainer}>
					<Button
						text={allText.buttonText[0].text}
						className={styles.secButton}
						variants={contentVariants}
					/>
				</div>
			</motion.div>
		</div>
	);
};

export default withSectionLayout(Section7);
