import React, { DetailedHTMLProps, HTMLAttributes } from 'react';
import styles from './Section12.module.scss';
import cn from 'classnames';
import withSectionLayout from '../../../HOC/withSectionLayout';
import { motion, Variants } from 'framer-motion';
// import { useInView } from 'react-intersection-observer';
import Link from 'next/link';

interface Section12Props
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}

const containerVariants: Variants = {
	shown: {
		y: [50, 50, 50, 0],
		transition: {
			times: [0, 0.95, 0.96, 1],
			staggerChildren: 0.4,
			duration: 4.0,
		},
	},
	hidden: { y: 50, transition: {} },
};
const wordAnimation: Variants = {
	hidden: {},
	shown: {
		transition: {
			staggerChildren: 0.05,
		},
	},
};
const letterAnimation: Variants = {
	hidden: {
		y: 400,
	},
	shown: {
		y: 0,
		transition: {
			ease: [0.6, 0.01, -0.05, 0.95],
			duration: 0.2,
		},
	},
};
const subHeaderAnimation: Variants = {
	initial: {
		opacity: 0,
	},
	hidden: {
		opacity: 0,
	},
	shown: {
		opacity: 1,
		transition: {
			delay: 4,
			duration: 0.5,
		},
	},
};

const Section12 = ({ className, ...props }: Section12Props): JSX.Element => {
	const text = 'Научитесь петь несмотря на все «поздно» и «не дано».';
	const wordsArray = text.split(' ').map((word, i) => {
		if (i === text.split(' ').length - 1) {
			return (
				<motion.span
					key={i}
					variants={wordAnimation}
					className={styles.word}
				>
					{[...word].map((letter, index) => (
						<motion.span key={index} variants={letterAnimation}>
							{letter}
						</motion.span>
					))}
				</motion.span>
			);
		}
		return (
			<motion.span
				key={i}
				variants={wordAnimation}
				className={styles.word}
			>
				{[...(word + ' ')].map((letter, index) => (
					<motion.span key={index} variants={letterAnimation}>
						{letter}
					</motion.span>
				))}
			</motion.span>
		);
	});

	// const [ref, inView] = useInView();

	return (
		<div className={cn(styles.section12, className)} {...props}>
			<motion.div
				className={styles.infoContainer}
				variants={containerVariants}
				// animate={inView ? 'shown' : 'hidden'}
				// initial={'hidden'}
				// ref={ref}
			>
				<motion.p
					className={styles.innerText1}
					variants={containerVariants}
					// animate={inView ? 'shown' : 'hidden'}
				>
					{wordsArray}
				</motion.p>
				<motion.p
					className={styles.innerText2}
					variants={subHeaderAnimation}
				>
					<Link href='tel:89108108884'><a className={styles.telLink}>8-910-810-88-84</a></Link>
				</motion.p>
			</motion.div>
		</div>
	);
};

export default withSectionLayout(Section12);
