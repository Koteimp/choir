import React, { DetailedHTMLProps, HTMLAttributes } from 'react';
import styles from './Section8.module.scss';
import cn from 'classnames';
import withSectionLayout from '../../../HOC/withSectionLayout';
import SectionHeader from '../../SectionHeader/SectionHeader';
import * as dataText from '../../../../Components/data';
import PhaseCard from '../../PhaseCard/PhaseCard';
import { motion, Variants } from 'framer-motion';
import { useInView } from 'react-intersection-observer';

interface Section8Props
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}

const containerVariants: Variants = {
	shown: {
		transition: {
			staggerChildren: 0.5,
			delayChildren: 0.5,
		},
	},
	hidden: { transition: {} },
};
const contentVariants: Variants = {
	shown: {
		y: '0px',
		opacity: 1,
		transition: { ease: [0.26, 0.67, 0.48, 0.91] },
	},
	hidden: { y: '100px', opacity: 0, transition: {} },
};

const Section8 = ({ className, ...props }: Section8Props): JSX.Element => {
	const allText = dataText.allText;
	const rm = { rootMargin: '50px 0px 0px 0px' };
	const [ref2, inView2] = useInView(rm);
	const [ref3, inView3] = useInView(rm);
	const [ref4, inView4] = useInView(rm);
	const [ref5, inView5] = useInView(rm);

	return (
		<div className={cn(styles.section8, className)} {...props}>
			<SectionHeader
				header={allText.sectionHeaders[7].header}
				text={allText.sectionHeaders[7].text}
				section='secondary'
				className={styles.sectionHeader}
			/>
			{/* //1й */}
			<div className={styles.cards}>
				<motion.div
					className={styles.cardContainer1}
					animate={inView2 ? 'shown' : 'hidden'}
					initial='hidden'
					ref={ref2}
					variants={containerVariants}
				>
					<motion.div
						className={styles.imageContainer}
						variants={contentVariants}
					>
						<div className={styles.square}></div>
					</motion.div>
					<motion.div
						className={styles.phaseCard}
						variants={contentVariants}
					>
						<PhaseCard
							header={allText.phases[0].header}
							list={allText.phases[0].listElements}
							noteExists={false}
							className={styles.card}
						/>
					</motion.div>
				</motion.div>
				{/* //2й */}
				<motion.div
					className={styles.cardContainer2}
					animate={inView3 ? 'shown' : 'hidden'}
					initial='hidden'
					ref={ref3}
					variants={containerVariants}
				>
					<motion.div
						className={styles.imageContainer}
						variants={contentVariants}
					>
						<div className={styles.square}></div>
					</motion.div>
					<motion.div
						className={styles.phaseCard}
						variants={contentVariants}
					>
						<PhaseCard
							header={allText.phases[1].header}
							list={allText.phases[1].listElements}
							noteExists={false}
							className={styles.card}
						/>
					</motion.div>
				</motion.div>
				{/* //3й */}
				<motion.div
					className={styles.cardContainer3}
					animate={inView4 ? 'shown' : 'hidden'}
					initial='hidden'
					ref={ref4}
					variants={containerVariants}
				>
					<motion.div
						className={styles.imageContainer}
						variants={contentVariants}
					>
						<div className={styles.square}></div>
					</motion.div>
					<motion.div
						className={styles.phaseCard}
						variants={contentVariants}
					>
						<PhaseCard
							header={allText.phases[2].header}
							list={allText.phases[2].listElements}
							noteExists={false}
							className={styles.card}
						/>
					</motion.div>
				</motion.div>
				{/* //4й */}
				<motion.div
					className={styles.cardContainer4}
					animate={inView5 ? 'shown' : 'hidden'}
					initial='hidden'
					ref={ref5}
					variants={containerVariants}
				>
					<motion.div
						className={styles.imageContainer}
						variants={contentVariants}
					>
						<div className={styles.square}></div>
					</motion.div>
					<motion.div
						className={styles.phaseCard}
						variants={contentVariants}
					>
						<PhaseCard
							header={allText.phases[3].header}
							list={allText.phases[3].listElements}
							note={allText.phases[3].note}
							noteExists={true}
							className={styles.card}
						/>
					</motion.div>
				</motion.div>
			</div>
		</div>
	);
};

export default withSectionLayout(Section8);
