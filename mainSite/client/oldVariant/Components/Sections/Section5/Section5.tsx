import React, { DetailedHTMLProps, HTMLAttributes } from 'react';
import styles from './Section5.module.scss';
import cn from 'classnames';
import withSectionLayout from '../../../HOC/withSectionLayout';
import SectionHeader from '../../SectionHeader/SectionHeader';
import * as dataText from '../../../../Components/data';
import { useInView } from 'react-intersection-observer';
import { motion, Variants } from 'framer-motion';

interface Section5Props
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}

const containerVariants: Variants = {
	shown: {
		transition: {
			staggerChildren: 0.5,
			delayChildren: 0.5,
		},
	},
	hidden: { transition: {} },
};

const containerVariants2: Variants = {
	shown: {
		transition: {
			staggerChildren: 0.5,
		},
	},
	hidden: { transition: {} },
};
const contentVariants: Variants = {
	shown: {
		opacity: 1,
		y: '0px',
		transition: {
			ease: 'easeOut',
		},
	},
	hidden: { opacity: 0, y: '50px', transition: {} },
};

const Section5 = ({ className, ...props }: Section5Props): JSX.Element => {
	const allText = dataText.allText;
	const [ref2, inView2] = useInView();

	return (
		<div className={cn(styles.section5, className)} {...props}>
			<SectionHeader
				header={allText.sectionHeaders[4].header}
				section='secondary'
				className={styles.sectionHeader}
			/>
			<motion.div
				className={styles.contentContainer}
				animate={inView2 ? 'shown' : 'hidden'}
				initial='hidden'
				ref={ref2}
				variants={containerVariants}
			>
				<motion.div
					className={styles.textContainer}
					variants={containerVariants2}
				>
					<motion.p
						className={styles.quote}
						variants={contentVariants}
					>
						&quot;
					</motion.p>
					<motion.p
						className={styles.text}
						variants={contentVariants}
					>
						Пение в православном хоре - это молитва. Прихожане храма
						тонко чувствуют когда поют искренне. Поэтому очень важно
						петь правильно. Я помогу этому научиться.{' '}
					</motion.p>
					<motion.p
						className={styles.author}
						variants={contentVariants}
					>
						Виктор Эдуардович Пилецкий
					</motion.p>
					<motion.p
						className={styles.director}
						variants={contentVariants}
					>
						Регент (руководитель) церковного хора
					</motion.p>
				</motion.div>
				<motion.div
					className={styles.imageContainer}
					variants={contentVariants}
				>
					<div className={styles.circle}></div>
				</motion.div>
			</motion.div>
		</div>
	);
};

export default withSectionLayout(Section5);
