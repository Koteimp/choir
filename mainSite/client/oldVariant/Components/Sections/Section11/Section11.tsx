import React, { DetailedHTMLProps, HTMLAttributes } from 'react';
import styles from './Section11.module.scss';
import cn from 'classnames';
import withSectionLayout from '../../../HOC/withSectionLayout';
import * as dataText from '../../../../Components/data';
import SectionHeader from '../../SectionHeader/SectionHeader';
import Tabs from '../../Tabs/Tabs';
import Accordion from '../../Accordion/Accordion';
import windowSize from '../../../hooks/useWindowSize';
// import { useInView } from 'react-intersection-observer';

interface Section11Props
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}

const Section11 = ({ className, ...props }: Section11Props): JSX.Element => {
	// const tabs = dataText.tabs;
	// const allText = dataText.allText;
	// const size = windowSize();
	// const [ref1, inView1] = useInView();

	return (
		<div className={cn(styles.section11, className)} {...props}>
			{/* <SectionHeader
				header={allText.sectionHeaders[8].header}
				section='secondary'
				className={styles.sectionHeader}
				shouldAnimate={false}
			/>
			<div className={styles.tabsContainer}>
				{size > 767 ? <Tabs tabs={tabs} /> : <Accordion tabs={tabs} />}
			</div> */}
		</div>
	);
};

export default withSectionLayout(Section11);
