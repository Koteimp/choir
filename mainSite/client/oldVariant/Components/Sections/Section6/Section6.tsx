import React from 'react';
import styles from './Section6.module.scss';
import cn from 'classnames';
import withSectionLayout from '../../../HOC/withSectionLayout';
import SectionHeader from '../../SectionHeader/SectionHeader';
import * as dataText from '../../../../Components/data';
import SkillsSteps from '../../SkillsSteps/SkillsSteps';
import { useInView } from 'react-intersection-observer';
import {
	AnimatePresence,
	HTMLMotionProps,
	motion,
	Variants,
} from 'framer-motion';

interface Section6Props extends HTMLMotionProps<'div'> {}

const cardContainerVariants: Variants = {
	shown: {
		transition: {
			staggerChildren: 0.3,
			delayChildren: 0.5,
		},
	},
	hidden: { transition: {} },
};

const Section6 = ({ className, ...props }: Section6Props): JSX.Element => {
	const allText = dataText.allText;
	const [ref2, inView2] = useInView();

	return (
		<motion.div className={cn(styles.section6, className)} {...props}>
			<SectionHeader
				header={allText.sectionHeaders[5].header}
				section='secondary'
				className={styles.sectionHeader}
			/>
			<AnimatePresence>
				<motion.div
					className={styles.cardContainer}
					variants={cardContainerVariants}
					ref={ref2}
					animate={inView2 ? 'shown' : 'hidden'}
					initial={'hidden'}
				>
					{allText.skillsText.map((item, index) => (
						<SkillsSteps
							key={index}
							text={item.text}
							header={item.header}
							skill
						/>
					))}
				</motion.div>
			</AnimatePresence>
		</motion.div>
	);
};

export default withSectionLayout(Section6);
