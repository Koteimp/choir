import React, {
	ForwardedRef,
	forwardRef,
} from 'react';
import styles from './Button.module.scss';
import cn from 'classnames';
import { HTMLMotionProps, motion, Variants } from 'framer-motion';
import Link from 'next/link';

// extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
interface ButtonProps extends HTMLMotionProps<'button'> {
	text: string;
}

const hoverVariants: Variants = {
	rest: {
		y: 0,
		transition: {},
	},
	hover: {
		y: -3,
		transition: {},
	},
};

const Button = (
	{ text, className, ...props }: ButtonProps,
	ref: ForwardedRef<HTMLButtonElement>
): JSX.Element => {
	return (
		<motion.button
			className={cn(styles.button, className)}
			variants={hoverVariants}
			initial='rest'
			whileHover='hover'
			animate='rest'
			ref={ref}
			{...props}
		>
			<Link href={'tel:223234234324234'}>
				<a className={styles.buttonText}>{text}</a>
			</Link>
		</motion.button>
	);
};

export default motion(forwardRef(Button));
