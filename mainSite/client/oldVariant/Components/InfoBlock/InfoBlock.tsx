import React, { DetailedHTMLProps, HTMLAttributes } from 'react';
import styles from './InfoBlock.module.scss';
import cn from 'classnames';

interface InfoBlockProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	text: string;
	header: string;
}

const InfoBlock = ({ text, header, className, ...props }: InfoBlockProps) => {
	return (
		<div className={cn(styles.infoBlock, className)} {...props}>
			<div className={styles.infoBlockContainer}>
				<p className={styles.infoHeader}>{header}</p>
				<p className={styles.infoText}>{text}</p>
			</div>
		</div>
	);
};

export default InfoBlock;
