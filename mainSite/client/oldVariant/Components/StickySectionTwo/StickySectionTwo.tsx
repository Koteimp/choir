import React, { useContext, useEffect, useRef } from 'react';
import styles from './StickySectionTwo.module.scss';
import * as dataText from '../../../Components/data';
import { motion } from 'framer-motion';
import { useTransform, useViewportScroll } from 'framer-motion';
import SectionHeader from '../SectionHeader/SectionHeader';
import Card from '../Card/Card';
import useWindowSize from '../../hooks/useWindowSize';

interface StickySectionProps {}

const StickySection2 = ({ ...props }: StickySectionProps): JSX.Element => {
	const size = useWindowSize();
	const allText = dataText.allText;
	//высота секции
	let a = 1000;

	const heightMax = 3000 + a;

	const wrapperRef = useRef<HTMLDivElement | any>(null);
	const contentRef = useRef<HTMLDivElement | any>(null);
	const cardsRef = useRef<any | any>();
	useEffect(() => {
		if (wrapperRef && contentRef) {
			const height = contentRef.current.offsetHeight + heightMax;
			wrapperRef.current.style.height = `${height}px`;
		}
	}, [wrapperRef, contentRef, heightMax]);
	let startPercent = 0;
	let endPercent = 0;
	let cardPercent = 0;
	//анимация при скролле контейнер идет снизу вверх
	const { scrollYProgress } = useViewportScroll();
	const yScroll = useTransform(
		scrollYProgress,
		[endPercent - startPercent / 2, endPercent - 0.02],
		['0%', '100%']
	);
	const opacityCard = useTransform(
		scrollYProgress,
		[startPercent, endPercent - startPercent / 2],
		[0, 1]
	);
	const transformCard1 = useTransform(
		scrollYProgress,
		[startPercent, endPercent - startPercent / 2],
		['-115%', '0%']
	);
	const transformCard2 = useTransform(
		scrollYProgress,
		[startPercent, endPercent - startPercent / 2],
		['-225%', '0%']
	);
	const transformCard3 = useTransform(
		scrollYProgress,
		[startPercent, endPercent - startPercent / 2],
		['-340%', '0%']
	);
	//анимация фреймов при попадании в поле видимости
	const isMobile = size < 991 ? true : false;

	return (
		<div
			className={styles.stickySectionWrapper}
			ref={wrapperRef}
			{...props}
		>
			<div className={styles.stickySectionlContent} ref={contentRef}>
				<div className={styles.bgWrap}>
					<img alt='forest' src='/mountains.jpg' />
				</div>
				<motion.div className={styles.content} style={{ y: yScroll }}>
					<div className={styles.frame1}>
						<motion.div className={styles.frameContent}>
							<SectionHeader
								text={allText.sectionHeaders[3].text}
								header={allText.sectionHeaders[3].header}
								section='secondary'
								className={styles.sectionHeader}
							/>
							<motion.div
								className={styles.cardContainer}
								layout
								initial={{ borderRadius: 20 }}
								ref={cardsRef}
							>
								<Card
									text={allText.cardText[0].text}
									header={allText.cardText[0].header}
									darkText={allText.cardText[0].darkText}
									className={styles.card1}
									style={
										isMobile
											? {
													y: transformCard1,
													opacity: opacityCard,
											  }
											: {
													x: transformCard1,
													opacity: opacityCard,
											  }
									}
								/>
								<Card
									text={allText.cardText[1].text}
									header={allText.cardText[1].header}
									darkText={allText.cardText[1].darkText}
									className={styles.card2}
									style={
										isMobile
											? {
													y: transformCard2,
													opacity: opacityCard,
											  }
											: {
													x: transformCard2,
													opacity: opacityCard,
											  }
									}
								/>
								<Card
									text={allText.cardText[2].text}
									header={allText.cardText[2].header}
									darkText={allText.cardText[2].darkText}
									className={styles.card3}
									style={
										isMobile
											? {
													y: transformCard3,
													opacity: opacityCard,
											  }
											: {
													x: transformCard3,
													opacity: opacityCard,
											  }
									}
								/>
							</motion.div>
						</motion.div>
					</div>
				</motion.div>
			</div>
		</div>
	);
};

export default StickySection2;
