import React, { useState } from 'react';
import {
	motion,
	AnimatePresence,
	HTMLMotionProps,
	Variants,
} from 'framer-motion';
import { ITabs } from '../../../Components/data';
import styles from './Accordion.module.scss';

interface AccordionProps extends HTMLMotionProps<'div'> {
	tabs: ITabs[];
}

const variants: Variants = {
	active: {
		color: 'var(--colorM)',
	},
	closed: {
		color: 'var(--colorMD)',
	},
};
const variantsContent: Variants = {
	active: {
		height: 'auto',
	},
	closed: {
		height: '0px',
	},
};
const variantsArrow: Variants = {
	active: {
		transform: 'rotate(0deg)',
		transition: { duration: 0.5 },
	},
	closed: {
		transform: 'rotate(-180deg)',
		transition: { duration: 0.5 },
	},
};

const Accordion = ({
	tabs,
	className,
	...props
}: AccordionProps): JSX.Element => {
	const [active, setActive] = useState<number | boolean>(false);

	return (
		<>
			{tabs.map((item, i) => {
				const isActive = i === active;
				return (
					<motion.div
						className={styles.accordionContainer}
						{...props}
						key={item.id}
					>
						<motion.div
							className={styles.accordionTitleContainer}
							initial={false}
							animate={isActive ? 'active' : 'closed'}
							variants={variants}
							transition={{ duration: 0.5 }}
							onClick={() => setActive(isActive ? false : i)}
						>
							<motion.p
								className={styles.accordionTitle}
								variants={variants}
								animate={isActive ? 'active' : 'closed'}
							>
								{item.title}
							</motion.p>
							<motion.span
								className={styles.arrowContainer}
								animate={!isActive ? 'active' : 'closed'}
								variants={variantsArrow}
							>
								<svg
									stroke='currentColor'
									fill='currentColor'
									strokeWidth='0'
									viewBox='0 0 512 512'
									height='1em'
									width='1em'
								>
									<path
										fill='none'
										strokeLinecap='round'
										strokeLinejoin='round'
										strokeWidth='48'
										d='M112 184l144 144 144-144'
									></path>
								</svg>
							</motion.span>
						</motion.div>
						<AnimatePresence exitBeforeEnter>
							{isActive && (
								<motion.div
									key={item.id}
									className={styles.accordionTextContainer}
									initial='closed'
									animate='active'
									exit='closed'
									variants={variantsContent}
									transition={{
										duration: 0.5,
										type: 'tween',
									}}
								>
									<p className={styles.accordionContentText}>
										{item.content.text}
									</p>
									<p
										className={
											styles.accordionContentDarkText
										}
									>
										{item.content.darkText}
									</p>
								</motion.div>
							)}
						</AnimatePresence>
					</motion.div>
				);
			})}
		</>
	);
};

export default Accordion;
