import React, { DetailedHTMLProps, HTMLAttributes } from 'react';
import styles from './Header.module.scss';
import cn from 'classnames';
import Link from 'next/link';

interface HeaderProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement> {}

const Header = ({ className, ...props }: HeaderProps): JSX.Element => {
	return (
		<header className={cn(styles.header, className)} {...props}>
			<div className={styles.contentWrapper}>
				<div className={styles.content}>
					<div className={styles.logo}>
						<span className={styles.logoText1}>Православный</span>
						<span className={styles.logoText2}>хор Пилецкого</span>
					</div>
					<div className={styles.linkContainer}>
						<Link href='tel:89108108884'>
							<a className={styles.link}>8-910-810-88-84</a>
						</Link>
					</div>
				</div>
			</div>
		</header>
	);
};

export default Header;
