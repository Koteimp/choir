import React, { useState } from 'react';
import styles from './Tabs.module.scss';
import cn from 'classnames';
import {
	AnimatePresence,
	HTMLMotionProps,
	motion,
	Variants,
	AnimateSharedLayout,
} from 'framer-motion';
import { ITabs } from '../../../Components/data';

interface TabsProps extends HTMLMotionProps<'div'> {
	tabs: ITabs[];
	defaultIndex?: number;
}

const variantsLink: Variants = {
	active: {
		color: 'var(--colorM)',
	},
	inactive: {
		color: 'var(--colorMD)',
	},
};

const variantsTabContent: Variants = {
	initial: { opacity: 0, y: 20 },
	active: {
		opacity: 1,
		y: 0,
	},
	exit: {
		opacity: 0,
		y: -20,
	},
};

const Tabs = ({
	tabs,
	defaultIndex = 0,
	className,
	...props
}: TabsProps): JSX.Element => {
	const [selectedTab, setSelectedTab] = useState(tabs[0]);
	const onTabClick = (item: ITabs) => {
		setSelectedTab(item);
	};

	return (
		<motion.div className={cn(styles.tabComponent, className)} {...props}>
			<AnimateSharedLayout>
				<ul className={styles.tabLinks}>
					{tabs.map((item) => (
						<motion.li
							key={item.id}
							className={styles.tab}
							animate={
								selectedTab === item ? 'active' : 'inactive'
							}
							variants={variantsLink}
						>
							<button className={styles.tabLink}>
								<motion.span
									onClick={() => onTabClick(item)}
									className={styles.tabTitle}
									animate={
										selectedTab === item
											? 'active'
											: 'inactive'
									}
									variants={variantsLink}
								>
									{item.title}
								</motion.span>
							</button>
							{selectedTab === item ? (
								<motion.div
									className={styles.underline}
									layoutId='underline'
									transition={{
										type: 'spring',
										duration: 0.5,
									}}
								/>
							) : null}
						</motion.li>
					))}
				</ul>
			</AnimateSharedLayout>
			<div  className={styles.lineContainer}>
				<span className={styles.underline2}></span>
			</div>
			<AnimatePresence exitBeforeEnter>
				<motion.div
					key={selectedTab ? selectedTab.id : 'empty'}
					className={styles.tabContent}
					animate={'active'}
					initial={'initial'}
					exit={'exit'}
					transition={{ duration: 0.5, type: 'spring' }}
					variants={variantsTabContent}
				>
					<p className={styles.tabContentTitle}>
						{selectedTab ? selectedTab.content.title : ''}
					</p>
					<p className={styles.tabContentText}>
						{selectedTab ? selectedTab.content.text : ''}
					</p>
					<p className={styles.tabContentDarkText}>
						{selectedTab ? selectedTab.content.darkText : ''}
					</p>
				</motion.div>
			</AnimatePresence>
		</motion.div>
	);
};

export default Tabs;
